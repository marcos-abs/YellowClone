# Yellow App Clone

App feito com Xamarin.Forms para a talk "**O Poder do XAML, criando interfaces incríveis!**". 

Link da live - Parte I https://www.youtube.com/watch?v=x_sNPEwS3kA

Alguns prints do app:

#### Map

<img src="art/map_android.png" height="500" /> <img src="art/map_ios.png" height="500" />

#### Menu

<img src="art/menu_android.png" height="500" /> <img src="art/menu_ios.png" height="500" />

#### My Wallet

<img src="art/wallet_android.png" height="500" /> <img src="art/wallet_ios.png" height="500" />

#### Promotions

<img src="art/promotion_android.png" height="500" /> <img src="art/promotion_ios.png" height="500" />

#### My Trips

<img src="art/trip_android.png" height="500" /> <img src="art/trip_ios.png" height="500" />